// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <math.h>
#include "md2model.h"

#include "rt3dObjLoader.h"
#include <stack>          // std::stack

// md2 stuff
md2model tmpModel;
int currentAnim = 0;
GLuint md2VertCount = 0;


using namespace std;

// Globals
// Real programs don't use globals :-D
// Data would normally be read from files
GLfloat vertices[] = {	-1.0f,0.0f,0.0f,
						0.0f,1.0f,0.0f,
						0.0f,0.0f,0.0f, };
GLfloat colours[] = {	1.0f, 0.0f, 0.0f,
						0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 1.0f };
GLfloat vertices2[] = {	0.0f,0.0f,0.0f,
						0.0f,-1.0f,0.0f,
						1.0f,0.0f,0.0f };

float dx = 0.0f, dy = 0.0f, dz = 0.0f, r = 0.0f;

rt3d::lightStruct light0 = {
		{ 0.5f, 0.5f, 0.5f, 1.0f }, // ambient
		{ 1.0f, 1.0f, 1.0f, 1.0f }, // diffuse
		{ 1.0f, 1.0f, 1.0f, 1.0f }, // specular
		{ -5.0f, 2.0f, 2.0f, 1.0f }  // position
};
glm::vec4 lightPos(-5.0f, 2.0f, 2.0f, 1.0f); //light position

rt3d::materialStruct material0 = {
		{ 0.5f, 0.5f, 0.5f, 1.0f }, // ambient
		{ 1.0f, 1.0f, 1.0f, 1.0f }, // diffuse
		{ 0.5f, 0.5f, 0.0f, 1.0f }, // specular
		2.0f  // shininess
};

glm::vec3 housePos = glm::vec3(-2.0f, -1.2f, -6.0f); // house position
float housedx = 0.1f;

// remember to increase the number of objects... one for each mesh to be loaded
GLuint meshObjects[3];

GLuint mvpShaderProgram;
glm::mat4 MVP;

// Texture and lighting
GLuint textures[3];
GLuint skyboxTex[6];

// new
int meshIndexCount[2];
GLuint litShaderProgram;
GLuint textureShaderProgram;

// A simple texture loading function
// lots of room for improvement - and better error checking!
GLuint loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID; // return value of texture ID
}


// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void initSkybox() {
	skyboxTex[0] = loadBitmap("posz.bmp"); // front
	skyboxTex[1] = loadBitmap("negz.bmp"); // back
	skyboxTex[2] = loadBitmap("negx.bmp"); // left
	skyboxTex[3] = loadBitmap("posx.bmp"); // right
	skyboxTex[4] = loadBitmap("posy.bmp"); // top
	skyboxTex[5] = loadBitmap("negy.bmp"); // bottom
}

void init(void) {

#if _DEBUG
	std::cout << "Hello debug!" << endl;
#endif

	glEnable(GL_DEPTH_TEST); // enable depth testing
	// For this simple example we'll be using the most basic of shader programs
	mvpShaderProgram = rt3d::initShaders("mvp.vert", "minimal.frag");
	MVP = glm::mat4(1.0); // init to identity matrix

	litShaderProgram = rt3d::initShaders("phong-tex.vert", "phong-tex.frag");
	textureShaderProgram = rt3d::initShaders("textured.vert", "textured.frag");
	//litShaderProgram = rt3d::initShaders("gouraud.vert", "simple.frag");
	// delete
	//meshObjects[1] = rt3d::createMesh(3, vertices2);

	// new
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj("house.obj", verts, norms, tex_coords, indices);
	meshIndexCount[0] = indices.size();
	meshObjects[0] = rt3d::createMesh(verts.size() / 3, verts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCount[0], indices.data());

	verts.clear(); norms.clear(); tex_coords.clear(); indices.clear();
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);
	meshIndexCount[1] = indices.size();
	meshObjects[1] = rt3d::createMesh(verts.size() / 3, verts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCount[1], indices.data());

	textures[0] = loadBitmap("house_diffuse_flipV.bmp");
	textures[1] = loadBitmap("studdedmetal.bmp");

	// New lines for loading MD2 model and texture
	textures[2] = loadBitmap("hobgoblin2.bmp");
	meshObjects[2] = tmpModel.ReadMD2Model("hobgoblin.MD2");
	md2VertCount = tmpModel.getVertDataCount();


	initSkybox();

}

void update(void) {
	// currently only the forward 'w' and turn operations work correctly
	// update for other directions
	float startx = dx, starty = dy, startz = dz;
	if ( (housePos.x > 2.0f) || (housePos.x < - 6.0f) )
		housedx = - housedx;
	housePos.x += housedx;

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if (keys[SDL_SCANCODE_W]) {
		dz += 0.1 * cos(r);
		dx += 0.1 * -sin(r);
	}
	if (keys[SDL_SCANCODE_S]) dz -= 0.1;
	if (keys[SDL_SCANCODE_A]) {
		dx += 0.1;
	}
	if (keys[SDL_SCANCODE_D]) dx -= 0.1;
	if (keys[SDL_SCANCODE_R]) dy -= 0.1;
	if (keys[SDL_SCANCODE_F]) dy += 0.1;
	if (keys[SDL_SCANCODE_COMMA]) r -= 0.1f;
	if (keys[SDL_SCANCODE_PERIOD]) r += 0.1f;

	// Test for collision
	// if there is a collision, then we will reset dx,dy,dz to initial values
	float d = std::sqrt( std::powf(dx + housePos.x,2.0f) + std::powf(dz + housePos.z,2.0f) );
	std::cout << "Distance: " << d << endl;
	if (d < 1.3f) { // if distance is less than sum of radii, should be calculated
		dx = startx; dz = startz;
	}

	// additional keys to change the current animation - use these to see the range of animations available
	if (keys[SDL_SCANCODE_Z]) {
		if (--currentAnim < 0) currentAnim = 19;
		cout << "Current animation: " << currentAnim << endl;
	}
	if (keys[SDL_SCANCODE_X]) {
		if (++currentAnim >= 20) currentAnim = 0;
		cout << "Current animation: " << currentAnim << endl;
	}



}

void renderSkyBox(glm::mat4 & mv) {
	// to do
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mv);
	glm::mat4 modelview = glm::mat4(mvRotOnlyMat3);

	rt3d::setUniformMatrix4fv(textureShaderProgram, "modelview", glm::value_ptr(modelview));

	glDepthMask(GL_FALSE); // dont write to depth buffer
	std::stack<glm::mat4> mvStack;
	mvStack.push(modelview);

	// front
	mvStack.push(mvStack.top());
	glBindTexture(GL_TEXTURE_2D, skyboxTex[0]);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -2.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(2.0f, 2.0f, 0.0f));
	rt3d::setUniformMatrix4fv(textureShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();

	// back
	mvStack.push(mvStack.top());
	glBindTexture(GL_TEXTURE_2D, skyboxTex[1]);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 2.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(2.0f, 2.0f, 0.0f));
	rt3d::setUniformMatrix4fv(textureShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();

	// left
	mvStack.push(mvStack.top());
	glBindTexture(GL_TEXTURE_2D, skyboxTex[2]);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(0.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv(textureShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();

	// right
	mvStack.push(mvStack.top());
	glBindTexture(GL_TEXTURE_2D, skyboxTex[3]);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(0.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv(textureShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();

	mvStack.pop();
	glDepthMask(GL_TRUE); // re-enable write to depth buffer
	glEnable(GL_CULL_FACE);
}

void draw(SDL_Window * window) {
	// clear the screen
	static float rot = 0.0f;
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 projection = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 50.0f);
	glUseProgram(textureShaderProgram);
	rt3d::setUniformMatrix4fv(textureShaderProgram, "projection", glm::value_ptr(projection));

	std::stack<glm::mat4> mvStack;
	glm::mat4 modelview(1.0);
	// scene 'root'
	mvStack.push(modelview);
//	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(dx, dy, -4.0f + dz));
//	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::vec3 at = glm::vec3(-dx, -dy-0.5f, -dz);
	mvStack.top() = glm::lookAt(glm::vec3(-dx - 2* sin(r), -dy, -dz + 2 * cos(r) ), at, glm::vec3(0.0f, 1.0f, 0.0f));


	// draw sky box
	renderSkyBox(mvStack.top());

	glUseProgram(litShaderProgram);
	rt3d::setLightPos(litShaderProgram, glm::value_ptr(lightPos));
	rt3d::setUniformMatrix4fv(litShaderProgram, "projection", glm::value_ptr(projection));
	rt3d::setLight(litShaderProgram, light0);
	rt3d::setMaterial(litShaderProgram, material0);

	// draw a cube for ground plane
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-10.0f, -1.2f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(20.0f, 0.1f, 20.0f));
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	rt3d::setUniformMatrix4fv(litShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	//rt3d::setMaterial(shaderProgram, material1);
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();
	// end ground plane
	
	// draw a cube for 'player'
	mvStack.push(mvStack.top());
	// player is positioned at the 'at' position previously calculated
	mvStack.top() = glm::translate(mvStack.top(), at);
	// r is angle in radians - need to rotate player; GLM uses degrees so multiply up...
	mvStack.top() = glm::rotate(mvStack.top(), -r * 57.2957795f, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(0.25f, 0.25f, 0.25f));
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	rt3d::setUniformMatrix4fv(litShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	//rt3d::setMaterial(shaderProgram, material1);
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	mvStack.pop();


	//house 
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), housePos);
	// The house mesh is to a very different scale: scale to scene here immediately before render
	//mvStack.top() = glm::scale(mvStack.top(), glm::vec3(0.03));
	rot += 5.0f;
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(1.0f, 1.0f, 1.0f)); 

	rt3d::setUniformMatrix4fv(litShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	rt3d::drawIndexedMesh(meshObjects[1], meshIndexCount[1], GL_TRIANGLES);
	// end house
	mvStack.pop();

	// Animate the md2 model, and update the mesh with new vertex data
	tmpModel.Animate(currentAnim, 0.1);
	rt3d::updateMesh(meshObjects[2], RT3D_VERTEX, tmpModel.getAnimVerts(), tmpModel.getVertDataSize());

	// draw the animated model
	glCullFace(GL_FRONT); // md2 faces are defined clockwise, so cull front face
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	// could set a material here for this model if it is to use a different material
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.2f, -5.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(0.05f, 0.05f, 0.05f));
	rt3d::setUniformMatrix4fv(litShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(meshObjects[2], md2VertCount, GL_TRIANGLES);
	mvStack.pop();
	glCullFace(GL_BACK);
	mvStack.pop();

	SDL_GL_SwapWindow(window); // swap buffers
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}